export let employees = [
	
	{	
		id: 1,
		subdivision: 'IT',
		name: "Алексей",
		surname: "Сорокопуд",
		patronymic: "Иванович",
		post: "начальник"
	},
	{	
		id: 2,
		subdivision: 'IT',
		name: "Вадим",
		surname: "Колобчак",
		patronymic: "Петрович",
		post: "специалист"
	},
	{	
		id: 3,
		subdivision: 'Legal Department',
		name: "Юлия",
		surname: "Кратенко",
		patronymic: "Викторовна",
		post: "начальник"
	},
	{	
		id:4,
		subdivision: 'Legal Department',
		name: "Вадим",
		surname: "Таболин",
		patronymic: "Игоревич",
		post: "специалист"
	},
	{
		id: 5,
		subdivision: 'Bookkeeping',
		name: "Анна",
		surname: "Антонюк",
		patronymic: "Васильевна",
		post: "начальник"
	},
	{
		id: 6,
		subdivision: 'Bookkeeping',
		name: "Людмила",
		surname: "Бублик",
		patronymic: "Сергеевна",
		post: "ведущий специалист"
	},
	{
		id: 7,
		subdivision: 'IT',
		name: "Иван",
		surname: "Сидоров",
		patronymic: "Сидорович",
		post: "младший специалист",
	},
	{
		id: 8,
		subdivision: 'Legal Department',
		name: "Василий",
		surname: "Васильчук",
		patronymic: "Васильевич",
		post: "специалист",
	},
]
